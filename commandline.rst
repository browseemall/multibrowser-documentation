Command Line Interface
======================

You can run MultiBrowser from the command line to automate tasks and build server integration.::

Windows / Linux:

   MultiBrowser [Action] [Options]
   
MacOS:

    open MultiBrowser.Mac.app --args [Action] [Options]

Actions
-------

MultiBrowser knows 4 different actions:

**-start**

Launches MultiBrowser and automatically switches to the specified browser and url. You can choose not to specify browser and/or url. MultiBrowser will then use the standard-settings.

**-screenshot**

Launches MultiBrowser and automatically creates one or more screenshots depending on the options. After the screenshots have been created MultiBrowser will close automatically.

**-responsive**

Launches MultiBrowser and automatically creates screenshots for responsive layout verification in all desktop and mobile resolutions. After the screenshots have been created MultiBrowser will close automatically.

**-report**

Launches MultiBrowser and automatically creates a cross browser report. After the report has been created MultiBrowser will close automatically.

**-suite**

Opens an existing test suite.

**-runtests**

Opens an existing test suite and runs all tests using the browsers specified on the MultiBrowser line or in the suite.

Options
-------

Different options can be applied to these actions. These options are:

**Specify the URL for an action**

This option can be used to specify the url for any given action. Applies to: -start, -screenshot, -responsive, -report::

   -url:[URL]

*Example:*::

   -url:http://www.example.com

**Specify a single browser for an action**

This option can be used to specify a single browser for a given action. Applies to: -start, -screenshot, -report::

   -browser:[BROWSER]

*Example:*::

   -browser:CHROME41
   
**Specify more than one browser for an action**

This option can be used to specify more than one browser for a given action. Applies to: -screenshot, -report, -runtests::

   -browsers:[BROWSER1],[BROWSER2]

*Example:*::

   -browsers:CHROME41,FIREFOX37
   
**Specify the resolution for an action**

This option can be used to specify the resolution for a given action. The resolution can not be bigger than your native resolution. Applies to: -start, -screenshot::

   -resolution:[RESOLUTION]

*Example:*

   -resolution:1024x768
   
**Crop Screenshots**

This option can be used to tell MultiBrowser to crop the screenshot to the current or given resolution. Omitting this parameter will take a full page screenshot. Applies to: -screenshot::

   -snip
   
*Example:*::

   -snip
   
**Delay Screenshots**

This option can be used to tell MultiBrowser to delay taking of the screenshot for a number of given seconds. Omitting this parameter will take a screenshot without delay. Applies to: -screenshot::

   -delay:[NumberOfSeconds]
   
*Example:*::

   -delay:5
   
**Screenshots Comparison Tolerance**

This option can be used to tell MultiBrowser the tolerance used for layout comparison between screenshots in pixel. Applies to: -screenshot::

   -tolerance:[NumberOfPixels]
   
*Example:*::

   -tolerance:10
   
**Specify the data location**

This option can be used to tell MultiBrowser in which location the result of an action should be saved. Applies to: -screenshot, -responsive, -runtests::

   -saveto:[PATH]

*Example:*::

   -saveto:C:\Path\To\Folder\
   
**Specify a specific file location**

This option can be used to tell MultiBrowser to which file the result of an action should be saved. Applies to: -report::

   -savetofile:[PATH]
   
*Example:*::

   -savetofile:C:\Path\To\File.png
   
**Record Video**

This option can be used to tell MultiBrowser to record a video during automated test execution. Applies to: -runtests::

   -record
   
*Example:*::

   -record
   
Examples (Windows / Linux)
--------

**Starting MultiBrowser**::

   MultiBrowser -start -url:http://www.google.de -browser:IE9
   
**Generate screenshots**::

   MultiBrowser -screenshot -url:google.com -saveto:C:\Path -resolution:1280x1024 -browsers:IE8,IE9 -tolerance:10
   
**Generate report**::

   MultiBrowser -report -url:google.com -saveto:C:\Path -browsers:IE11,IE10
   
**Open a test suite**::

   MultiBrowser -suite:C:\Users\TestSuite.bts
   
**Running a test suite**::

   MultiBrowser -runtests:C:\Users\TestSuite.bts -browsers:CHROME62,FIREFOX57,IE10   
   
   
Examples (MacOS)
--------

**Starting MultiBrowser**::

   open MultiBrowser.Mac.app --args -start -url:http://www.google.de -browser:IE9
   
**Generate screenshots**::

   open MultiBrowser.Mac.app --args -screenshot -url:google.com -saveto:/Path/to/save -resolution:1280x1024 -browsers:IE8,IE9 -tolerance:10
   
**Generate report**::

   open MultiBrowser.Mac.app --args -report -url:google.com -saveto:/Path/to/save -browsers:IE11,IE10
   
**Open a test suite**::

   open MultiBrowser.Mac.app --args -suite:/Path/to/Suite/TestSuite.bts
   
**Running a test suite**::

   open MultiBrowser.Mac.app --args -runtests:/Path/to/Suite/TestSuite.bts   
   
**Running a test suite**::

   open MultiBrowser.Mac.app --args -runtests:/Path/to/Suite/TestSuite.bts -browsers:CHROME62,FIREFOX57,IE10      
   